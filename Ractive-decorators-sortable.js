/**
 * References:
 * - https://www.javascripttutorial.net/web-apis/javascript-drag-and-drop/
 * - https://web.dev/drag-and-drop/
 */
(function () {
    let dragSrcEl = null,
        dragSrcElParent = null;
    Ractive.decorators.sortable = function (node, dragOverCls, onBeforeDropEventEnabled, onDropEventEnabled, clearDragOverClassesOnDrop) {
        if (!Array.isArray(Ractive.getContext(node).get('../'))) {
            throw new Error('The sortable decorator only works with elements that correspond to array members');
        }
        const elWithGrabCls = node.classList.contains('cur-grab') ? node : node.querySelector('.cur-grab');
        if (elWithGrabCls !== null) {
            elWithGrabCls.setAttribute('title', 'Sırasını değiştirmek istiyorsanız dokunun, basılı tutun ve istediğiniz yere sürükleyin.');
        }
        dragOverCls = dragOverCls || 'abt-drag-over';
        node.draggable = true;
        node.addEventListener('dragstart', handleDragStart, false);
        node.addEventListener('dragenter', handleDragEnter, false);
        node.addEventListener('dragover', handleDragOver, false);
        node.addEventListener('dragleave', handleDragLeave, false);
        node.addEventListener('drop', handleDrop, false);
        node.addEventListener('dragend', handleDragEnd, false);
        return {
            teardown () {
                node.draggable = false;
                node.removeEventListener('dragstart', handleDragStart, false);
                node.removeEventListener('dragenter', handleDragEnter, false);
                node.removeEventListener('dragover', handleDragOver, false);
                node.removeEventListener('dragleave', handleDragLeave, false);
                node.removeEventListener('drop', handleDrop, false);
                node.removeEventListener('dragend', handleDragEnd, false);
            }
        };

        function parentsMatch (targetNode) {
            return targetNode.parentElement === dragSrcElParent;
        }

        function handleDragStart (e) {
            e.stopPropagation();
            if (e.target !== this) {
                e.preventDefault();
                return false;
            }
            const activeEl = document.activeElement;
            if (activeEl) {
                switch (activeEl.nodeName) {
                    case 'INPUT':
                    case 'TEXTAREA':
                    case 'SELECT':
                        e.preventDefault();
                        return false;
                }
            }
            dragSrcEl = this;
            dragSrcElParent = this.parentElement;
            this.style.opacity = '0.4';
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/html', this.innerHTML); // yalandan
        }

        function handleDragEnter (e) {
            e.stopPropagation();
            if (!parentsMatch(this)) {
                return false;
            }
            this.classList.add(dragOverCls);
        }

        function handleDragOver (e) {
            e.stopPropagation();
            if (!parentsMatch(this)) {
                return false;
            }
            if (e.preventDefault) {
                e.preventDefault();
            }
            e.dataTransfer.dropEffect = 'move';
            return false;
        }

        function handleDragLeave (e) {
            e.stopPropagation();
            if (!parentsMatch(this)) {
                return false;
            }
            if (
                e.relatedTarget === null ||
                // input'a drag edildiğinde böyle saçma bir şey oluyor. https://bugs.chromium.org/p/chromium/issues/detail?id=68629
                e.relatedTarget.parentElement === null ||
                e.currentTarget.contains(e.relatedTarget)
            ) {
                return false;
            }
            this.classList.remove(dragOverCls);
        }

        function handleDrop (e) {
            e.stopPropagation();
            if (!parentsMatch(this)) {
                return false;
            }
            if (this === dragSrcEl) {
                return false;
            }
            // Update model
            const context = Ractive.getContext(dragSrcEl);
            const ractive = context.ractive;
            const sourceItemKeypath = context.resolve();
            const targetItemKeypath = Ractive.getContext(this).resolve();
            const arrayKeypath = context.resolve('../');
            const sourceItem = ractive.get(sourceItemKeypath);
            const sourceIndex = Number(Ractive.splitKeypath(sourceItemKeypath).last);
            const targetIndex = Number(Ractive.splitKeypath(targetItemKeypath).last);
            if (onBeforeDropEventEnabled) {
                ractive.fire('Sortable.onBeforeDrop', context, sourceIndex, targetIndex, arrayKeypath);
            }
            const moveTask = Promise.all([
                ractive.splice(arrayKeypath, sourceIndex, 1),
                ractive.splice(arrayKeypath, targetIndex, 0, sourceItem)
            ]);
            if (onDropEventEnabled) {
                ractive.fire('Sortable.onDrop', context, sourceIndex, targetIndex, moveTask, arrayKeypath);
            }
            if (clearDragOverClassesOnDrop === true) {
                clearDragOverClasses();
            }
            return false;
        }

        function handleDragEnd (e) {
            e.stopPropagation();
            if (!parentsMatch(this)) {
                return false;
            }
            this.style.opacity = '1';
            clearDragOverClasses();
            dragSrcEl = dragSrcElParent = null;
        }

        function clearDragOverClasses () {
            [...dragSrcElParent.children].forEach(item => item.classList.remove(dragOverCls));
        }
    };
})();
