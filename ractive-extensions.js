//ş
Ractive.DEBUG = /unminified/.test(function(){/*unminified*/});

(function (r) {
    var data = r.defaults.data,
        EMPTY_STR = '';

    r.defaults.enhance = true;
    data.def = function (obj) { return obj !== undefined; };
    data.notNull = function (obj) { return obj !== null; };
    data.isNullOrEmpty = function (obj) { return obj === null || obj === EMPTY_STR; };

    if (typeof(moment) !== 'undefined') {
        data.moment = moment;
    }

    if (typeof(abt) !== 'undefined') {
        data.isEmpty = abt.isEmpty;
    }

    r.components.marginCollapseBlocker = r.extend({
        template : '<div class="abt-margin-collapse-blocker">&nbsp;</div>'
    });

    r.components.rinput = r.extend({
        template: `
            <input type="hidden" name="{{name}}" value="{{value}}" twoway="false">
            <input type="text" {{#if id !== null}}id="{{id}}"{{/if}} class="{{cls}}" readonly value="{{label}}" twoway="false">
        `,
        data: function () {
            return {
                id: null,
                name: null,
                value: null,
                label: '',
                cls: null
            };
        }
    });

    r.components.hourglass = r.extend({
        template : '<svg class="svgic svgic-hour-glass {{cls}}" role="img" aria-hidden="true" version="1.1" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M11.39 8c2.152-1.365 3.61-3.988 3.61-7 0-0.339-0.019-0.672-0.054-1h-13.891c-0.036 0.328-0.054 0.661-0.054 1 0 3.012 1.457 5.635 3.609 7-2.152 1.365-3.609 3.988-3.609 7 0 0.339 0.019 0.672 0.054 1h13.891c0.036-0.328 0.054-0.661 0.054-1 0-3.012-1.457-5.635-3.609-7zM2.5 15c0-2.921 1.253-5.397 3.5-6.214v-1.572c-2.247-0.817-3.5-3.294-3.5-6.214v0h11c0 2.921-1.253 5.397-3.5 6.214v1.572c2.247 0.817 3.5 3.294 3.5 6.214h-11zM9.682 10.462c-1.12-0.635-1.181-1.459-1.182-1.959v-1.004c0-0.5 0.059-1.327 1.184-1.963 0.602-0.349 1.122-0.88 1.516-1.537h-6.4c0.395 0.657 0.916 1.188 1.518 1.538 1.12 0.635 1.181 1.459 1.182 1.959v1.004c0 0.5-0.059 1.327-1.184 1.963-1.135 0.659-1.98 1.964-2.236 3.537h7.839c-0.256-1.574-1.102-2.879-2.238-3.538z"></path></svg>',
        data: function () {
            return {
                cls: null
            }
        }
    });

    r.components.LbSpinner = r.extend({
        template: '<svg class="svgic svgic-spinner2 {{baseCls}} spin {{cls}}" role="img" aria-hidden="true" version="1.1" width="16" height="16" viewBox="0 0 16 16"><path fill="currentColor" d="M16 8c-0.020-1.045-0.247-2.086-0.665-3.038-0.417-0.953-1.023-1.817-1.766-2.53s-1.624-1.278-2.578-1.651c-0.953-0.374-1.978-0.552-2.991-0.531-1.013 0.020-2.021 0.24-2.943 0.646-0.923 0.405-1.758 0.992-2.449 1.712s-1.237 1.574-1.597 2.497c-0.361 0.923-0.533 1.914-0.512 2.895 0.020 0.981 0.234 1.955 0.627 2.847 0.392 0.892 0.961 1.7 1.658 2.368s1.523 1.195 2.416 1.543c0.892 0.348 1.851 0.514 2.799 0.493 0.949-0.020 1.89-0.227 2.751-0.608 0.862-0.379 1.642-0.929 2.287-1.604s1.154-1.472 1.488-2.335c0.204-0.523 0.342-1.069 0.415-1.622 0.019 0.001 0.039 0.002 0.059 0.002 0.552 0 1-0.448 1-1 0-0.028-0.001-0.056-0.004-0.083h0.004zM14.411 10.655c-0.367 0.831-0.898 1.584-1.55 2.206s-1.422 1.112-2.254 1.434c-0.832 0.323-1.723 0.476-2.608 0.454-0.884-0.020-1.759-0.215-2.56-0.57-0.801-0.354-1.526-0.867-2.125-1.495s-1.071-1.371-1.38-2.173c-0.31-0.801-0.457-1.66-0.435-2.512s0.208-1.694 0.551-2.464c0.342-0.77 0.836-1.468 1.441-2.044s1.321-1.029 2.092-1.326c0.771-0.298 1.596-0.438 2.416-0.416s1.629 0.202 2.368 0.532c0.74 0.329 1.41 0.805 1.963 1.387s0.988 1.27 1.272 2.011c0.285 0.74 0.418 1.532 0.397 2.32h0.004c-0.002 0.027-0.004 0.055-0.004 0.083 0 0.516 0.39 0.94 0.892 0.994-0.097 0.544-0.258 1.075-0.481 1.578z"></path></svg>{{#if !iconOnly}}&nbsp;Lütfen bekleyin...{{/if}}',
        data: function () {
            return {
                baseCls: 'text-extra-muted',
                cls: null,
                iconOnly: false
            }
        }
    });

    r.components.yukleniyorStr = r.extend({
        template : 'Yükleniyor...'
    });

    r.components.RadioGroup = r.extend({
        template: '{{>content}}',
        isolated: false,
        data: () => ({
            name: '',
            values: [],
            selectedValue: '',
            required: false
        }),
        observe: {
            'values': {
                handler (newValues) {
                    const selectedValue = this.get('selectedValue');
                    if (selectedValue !== '' && !newValues.includes(selectedValue)) {
                        this.set('selectedValue', '');
                    }
                },
                strict: false,
                array: false,
                defer: true,
                init: false
            }
        }
    });

    r.decorators.radio = function (node, name, value, required, selectedValueKeypath) {
        const context = this.getContext(node);
        node.setAttribute('name', name);
        node.setAttribute('value', value);
        if (required) {
            node.setAttribute('required', '');
        }
        node.checked = value === context.get(selectedValueKeypath);
        node.addEventListener('change', handleChange);
        const observer = context.observe(selectedValueKeypath, function (newValue) { node.checked = value === newValue; }, {
            strict: true,
            array: false,
            defer: true,
            init: false
        });
        return {
            teardown () {
                observer.cancel();
                node.removeAttribute('name');
                node.removeAttribute('value');
                node.removeAttribute('required');
                node.removeEventListener('change', handleChange);
            }
        };
        function handleChange (e) {
            context.set(selectedValueKeypath, e.target.value);
        }
    };

    if (typeof r.prototype.clear !== 'undefined') {
        throw new Error('Ractive.prototype.clear is no longer available, please rename it!');
    }

    r.prototype.clear = function (keypath) {
        return this.splice(keypath, 0, this.get(keypath).length);
    }

    if (typeof r.prototype.pushAll !== 'undefined') {
        throw new Error('Ractive.prototype.pushAll is no longer available, please rename it!');
    }

    r.prototype.pushAll = function (keypath, items) {
        return this.push.apply(this, [keypath].concat(items));
    }
})(Ractive);
















